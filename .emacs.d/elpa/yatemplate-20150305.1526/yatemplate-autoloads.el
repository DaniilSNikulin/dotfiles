;;; yatemplate-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (yatemplate-fill-alist yatemplate-dir) "yatemplate"
;;;;;;  "yatemplate.el" (21758 3691 856055 753000))
;;; Generated autoloads from yatemplate.el

(defvar yatemplate-dir (locate-user-emacs-file "templates") "\
The directory containing file templates.")

(custom-autoload 'yatemplate-dir "yatemplate" t)

(autoload 'yatemplate-fill-alist "yatemplate" "\
Fill `auto-insert-alist'.

\(fn)" nil nil)

;;;***

;;;### (autoloads nil nil ("yatemplate-pkg.el") (21758 3691 936564
;;;;;;  906000))

;;;***

(provide 'yatemplate-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; yatemplate-autoloads.el ends here
